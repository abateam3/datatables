# README #

This is the datatable widget based on Meteor JS and JQuery

### List of unrealized features ###

* It doesn't allowed to search by the column.
* Material Design is not added as well as I want.

### How do I get set up? ###

* Simply create a new project at your computer by typing a command: meteor create project_name
* Go to the project folder by a command: cd project_name
* Install Materialize CSS Framework and Reactive Table module: meteor add materialize:materialize aslagle:reactive-table
* Copy the content of this repository to the folder of created project
* Start Meteor project: meteor (if you're running another Meteor project, type the number of port "--port X")
* Create Collection "users" and add some documents with the columns "login", "email", "gender", "firstName", "lastName", "group"
* Open Web Browser
* Type: http://localhost:X, where X is the number of port (default value is 3000)
* Enjoy! :)