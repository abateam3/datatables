import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.db.helpers({
   settings: function() {
        var collection = Users.find();

        return {
            collection: Users,
            rowsPerPage: 10,
            showFilter: true,
            fields: [
              { key: 'login', label: 'Логин' },
              { key: 'email', label: 'Электронная почта' },
              { key: 'gender', label: 'Пол' },
              { key: 'firstName', label: 'Имя' },
              { key: 'lastName', label: 'Фамилия' },
              { key: 'group', label: 'Группа' }
            ]
        };
    },
    test: function(item) {
  var qnt = item.qnt;
    switch (qnt) {
      case 0:
        return 'danger';
      case 1:
        return 'warning';
      default:
        return ''
    }
  }
});